package com.example.classactivity5;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView list_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String teamList[] =  {"Karachi Kings", "Lahore Qalandars", "Peshawar Zalmi", "Islamabad United", "Multan Sultans", "Quetta Gladiators"};
        int teamImages[] = {R.drawable.karachi_kings, R.drawable.lahore_qalandars, R.drawable.peshawar_zalmi, R.drawable.islamabad_united, R.drawable.multan_sultans, R.drawable.quetta_gladiators};

        ListView list_view = (ListView) findViewById(R.id.listView);

        // Instantiate the custom adapter class made
        PSLAdapter customAdapter = new PSLAdapter(getApplicationContext(), teamList, teamImages);
        list_view.setAdapter(customAdapter);
    }
}