package com.example.classactivity5;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PSLAdapter extends BaseAdapter {
    Context context;
    String team_list[];
    int team_images[];
    LayoutInflater test;

    public PSLAdapter(Context applicationContext, String[] teamList, int[] teamImages) {
        this.context = applicationContext;
        this.team_list = teamList;
        this.team_images = teamImages;
        this.test = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return team_list.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {

        // Instantiates a layout XML file into its corresponding View objects.
        view = test.inflate(R.layout.row, parent, false);

        // extract the text and image view id from the row.xml
        TextView team_name = (TextView) view.findViewById(R.id.textView);
        ImageView icon = (ImageView) view.findViewById(R.id.icon_image);

        team_name.setText(team_list[index]);
        icon.setImageResource(team_images[index]);

        // return the view after setting the icon image and text
        return view;
    }
}
